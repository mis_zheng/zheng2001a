import React from 'react'
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import {NavLink,Outlet} from "react-router-dom"
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import {useDispatch}from"react-redux"
import { HOMEFLAG } from "../../type/home.d"
const { Header, Content, Footer, Sider } = Layout;
type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuProps['items'] = [
  getItem(<NavLink to={"/home/xiaoxi"}>站内信息</NavLink>, 'sub1', <MailOutlined />, [
    getItem('全部消息', '全部消息', null,),
    getItem('已读消息', '已读消息', null, ),
    getItem('未读消息', '未读消息', null, ),
  ]),

  getItem(<NavLink to={"/home/shop"}>购买数据库</NavLink>, 'sub2', <AppstoreOutlined />, ),

 
];


function Home() {
  const dispatch=useDispatch()
  const onClick: MenuProps['onClick'] = e => {
        dispatch({
          type: HOMEFLAG,
          payload:e.key
        })
  };


  return (
    <Layout>
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={broken => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}
    >
      <div className="logo" />
      <Menu
      onClick={onClick}
      style={{ width: 256 }}
      defaultSelectedKeys={['1']}
      defaultOpenKeys={['sub1']}
      mode="inline"
      items={items}
    />
      {/* <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['4']}
        items={[{icon:UserOutlined,name:"购买数据库",path:"/home/shop"}, {icon:VideoCameraOutlined,name:"站内信",path:"/home/xiaoxi"}].map(
          (icon, index) => ({
            key: String(index + 1),
            icon: React.createElement(icon.icon),
            label: <NavLink to={icon.path}>{icon.name}</NavLink>,
          }),
        )}
      /> */}



    </Sider>
    <Layout>
      <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
      <Content style={{ margin: '24px 16px 0' }}>
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
         <Outlet></Outlet>
        </div>
      </Content>
  
    </Layout>
  </Layout>
  )
}

export default Home
